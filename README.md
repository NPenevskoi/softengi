# Mobile subscribers handling

System for mobile subscriber handling

## Getting Started

These instructions will get you a copy of the project up and running on your local machine for development and testing purposes. See deployment for notes on how to deploy the project on a live system.

### Prerequisites

What things you need to install the software and how to install them

1) Intellij IDEA 2017.3 ULTIMATE or any other appropriate IDE
2) Mysql SERVER 8(Optional, in case of using mysql server instead of h2 in-memory database)

### Installing

A step by step series of examples that tell you how to get an application is running

```
1. Download archive userhandling.zip
2. Unzip the project to appropriate directory
3. Build the broject with Maven
4. Run the project with the following command => java -Dserver.port=9090 -jar target/userhandling-0.0.1-SNAPSHOT.jar
```

## Running the tests

The below link show an example how to retrieve all users or particular user: 

```
http://localhost:8080/application/users
http://localhost:8080/application/users/3
```

Please note authentication user and password are the same => "admin"

## Deployment

Add additional notes about how to deploy this on a live system
1) Create build using Maven
2) Deploy created jar file to appropriated environment

## Docker(Building the image and run)
```
1) docker build -t hola-manual-build .
2) docker run -p 8080:8080 hola-manual-build or docker-compose up
```

## Built With

* [Maven](https://maven.apache.org/) - Dependency Management
* [Spring](https://start.spring.io/) - Used to generate sceleton for the project

## Authors

* **Nikolay Penevskoi** - (https://github.com/npenevskoi)

## Acknowledgments

* Hat tip to anyone whose code was used
* Inspiration
* etc

