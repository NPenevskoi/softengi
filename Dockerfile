# Sample Dockerfile
FROM openjdk:10-jre-slim
MAINTAINER npenevskoi
COPY target/userhandling-0.0.1-SNAPSHOT.jar /usr/src/hola/
WORKDIR /usr/src/hola

EXPOSE 8080
CMD ["java", "-jar", "userhandling-0.0.1-SNAPSHOT.jar"]