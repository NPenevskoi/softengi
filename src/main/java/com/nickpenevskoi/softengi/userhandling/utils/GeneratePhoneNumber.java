package com.nickpenevskoi.softengi.userhandling.utils;

import java.util.Arrays;
import java.util.List;
import java.util.Random;

public class GeneratePhoneNumber {
    public static String getRandomNumber() {
        Random rnd = new Random();
        String countryCode = "380";
        List<Integer> operatorCodes = Arrays.asList(67, 96, 97, 98, 50, 66, 95, 99, 63, 73, 93);
        int randomOperatorCode = operatorCodes.get(rnd.nextInt(operatorCodes.size()));
        int number = rnd.nextInt(9999999);

        return randomOperatorCode + String.format("%07d", number);
    }
}
