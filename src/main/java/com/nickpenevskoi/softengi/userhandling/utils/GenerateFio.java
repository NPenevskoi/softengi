package com.nickpenevskoi.softengi.userhandling.utils;

import com.github.javafaker.Faker;

public class GenerateFio {
    public static String getFIO() {
        Faker faker = new Faker();
        String name = faker.superhero().name();
        String surname = faker.artist().name();
        return name + " " + surname;
    }
}
