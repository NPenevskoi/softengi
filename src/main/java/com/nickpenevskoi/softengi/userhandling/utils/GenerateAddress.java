package com.nickpenevskoi.softengi.userhandling.utils;

import com.github.javafaker.Faker;

public class GenerateAddress {

    public static String getAddress() {
        Faker faker = new Faker();
        String streetName = faker.address().streetName();
        return streetName;
    }

}
