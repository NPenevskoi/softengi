package com.nickpenevskoi.softengi.userhandling.dao;

import com.nickpenevskoi.softengi.userhandling.entities.User;

import java.util.List;

public interface UserService {

    void saveUser(User user);

    void updateUser(User user);

    void deleteUserById(long id);

    void deleteAllUsers();

}
