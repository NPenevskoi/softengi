package com.nickpenevskoi.softengi.userhandling.dao;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.BeanPropertyRowMapper;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowMapper;

import com.nickpenevskoi.softengi.userhandling.entities.User;

public class UserDAO {
    @Autowired
    JdbcTemplate template;

    public void setTemplate(JdbcTemplate template) {
        this.template = template;
    }
    public int create(User p){
        String sql="insert into User(fio,address,phoneNumber) values('"+p.getFio()+"',"+p.getAddress()+",'"+p.getPhoneNumber()+"')";
        return template.update(sql);
    }
    public int update(User p){
        String sql="update User set fio='"+p.getFio()+"', address="+p.getAddress()+",phoneNumber='"+p.getPhoneNumber()+"' where id="+p.getId()+"";
        return template.update(sql);
    }
    public int delete(Long id){
        String sql="delete from User where id="+id+"";
        return template.update(sql);
    }
    public User getUsrById(Long id){
        String sql="select * from User where id=?";
        return template.queryForObject(sql, new Object[]{id},new BeanPropertyRowMapper<User>(User.class));
    }
    public List<User> getUsers(){
        return template.query("select * from User",new RowMapper<User>(){
            public User mapRow(ResultSet rs, int row) throws SQLException {
                User e=new User();
                e.setId(rs.getLong(1));
                e.setFio(rs.getString(2));
                e.setAddress(rs.getString(3));
                e.setPhoneNumber(rs.getString(4));
                return e;
            }
        });
    }

}
