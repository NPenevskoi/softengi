package com.nickpenevskoi.softengi.userhandling;

import com.nickpenevskoi.softengi.userhandling.entities.User;
import com.nickpenevskoi.softengi.userhandling.repository.UsersRepository;
import com.nickpenevskoi.softengi.userhandling.utils.GenerateAddress;
import com.nickpenevskoi.softengi.userhandling.utils.GenerateFio;
import com.nickpenevskoi.softengi.userhandling.utils.GeneratePhoneNumber;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;

import java.util.stream.LongStream;

@ComponentScan(basePackages = "com.nickpenevskoi.softengi.userhandling")
@SpringBootApplication
public class UserhandlingApplication {

	public static void main(String[] args) {
		SpringApplication.run(UserhandlingApplication.class, args);
	}

	@Bean
	CommandLineRunner init(UsersRepository repository) {
		return args -> {
			repository.deleteAll();
			LongStream.range(1, 50)
					.mapToObj(i -> {
                        User c = new User();
                        c.setPhoneNumber(GeneratePhoneNumber.getRandomNumber());
                        c.setFio(GenerateFio.getFIO());
                        c.setAddress(GenerateAddress.getAddress());

						return c;
					})
					.map(v -> repository.save(v))
					.forEach(System.out::println);
		};
	}



}
