package com.nickpenevskoi.softengi.userhandling.service;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.CompletableFuture;

import com.nickpenevskoi.softengi.userhandling.config.BasicAuthInterceptor;
import com.nickpenevskoi.softengi.userhandling.entities.User;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.http.client.ClientHttpRequestInterceptor;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

@Service
public class AsyncService {

    private static Logger log = LoggerFactory.getLogger(AsyncService.class);

    @Autowired
    private RestTemplate restTemplate;

    @Bean
    public RestTemplate restTemplate() {
        return new RestTemplate();
    }

    @Async("asyncExecutor")
    public CompletableFuture<User[]> getUsers() throws InterruptedException
    {
        log.info("getUsers starts");
        //Create and initialize the interceptor
        final List<ClientHttpRequestInterceptor> interceptors = new ArrayList<ClientHttpRequestInterceptor>();
        interceptors.add( new BasicAuthInterceptor( "admin", "admin" ) );
        restTemplate.setInterceptors(interceptors);

        User[] users = restTemplate.getForObject("http://localhost:9093/application/users", User[].class);
        log.info("users, {}", users);
        Thread.sleep(3000L);    //Intentional delay
        log.info("users completed");
        return CompletableFuture.completedFuture(users);
    }

}
