package com.nickpenevskoi.softengi.userhandling.controller;

import java.util.concurrent.CompletableFuture;
import java.util.concurrent.ExecutionException;

import com.nickpenevskoi.softengi.userhandling.entities.User;
import com.nickpenevskoi.softengi.userhandling.service.AsyncService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class AsyncController {

    private static Logger log = LoggerFactory.getLogger(AsyncController.class);

    @Autowired
    private AsyncService service;

    @RequestMapping(value = "/testAsynch", method = RequestMethod.GET)
    public CompletableFuture<User[]> testAsynch() throws InterruptedException, ExecutionException
    {
        log.info("testAsynch Start");
        CompletableFuture<User[]> users = service.getUsers();
        log.info("Users--> " + users.get());
        return users;
    }
}
