package com.nickpenevskoi.softengi.userhandling.controller;

import com.nickpenevskoi.softengi.userhandling.dao.UserService;
import com.nickpenevskoi.softengi.userhandling.entities.User;
import com.nickpenevskoi.softengi.userhandling.repository.UsersRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.annotation.CacheEvict;
import org.springframework.cache.annotation.CachePut;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.cache.annotation.Caching;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;
import java.util.concurrent.CompletableFuture;

@RestController
@RequestMapping(value = "/application")
public class UserResource {
    @Autowired
    private UsersRepository usersRepository;
    @Autowired
    UserService userService;
//-------------------Retrieve All Users---------------------------------------------------
    @Cacheable(value="usersCache")
    @RequestMapping(value = "/users", method = RequestMethod.GET)
    public List<User> retrieveAllUsers()
    {
        return usersRepository.findAll();
    }
//-------------------Retrieve Single User-------------------------------------------------
    @RequestMapping(value = "/users/{id}", method = RequestMethod.GET)
    public CompletableFuture<User> get(@PathVariable Long id){
        return usersRepository.findUserByID(id);
    }
//-------------------Create a User--------------------------------------------------------
    @Caching(put = {
            @CachePut(value="usersCache")
    })
    @RequestMapping(value = "/users/", method = RequestMethod.POST)
        public User createUser(@RequestBody User userAccount) {

        return usersRepository.save(userAccount);

    }
//------------------- Update a User ------------------------------------------------------
    @Caching(put = {
            @CachePut(value="usersCache")
    })
    @RequestMapping(value = "/users/{id}", method = RequestMethod.PUT)
    public ResponseEntity<User> updateUser(@PathVariable("id") long id, @RequestBody User currentUser) {
        User user = usersRepository.findUserByID_(id);
        if (user==null) {
           System.out.println("User with id " + id + " not found");
            return new ResponseEntity<User>(HttpStatus.NOT_FOUND);
       }

        usersRepository.save(currentUser);
        return new ResponseEntity<User>(HttpStatus.OK);
    }
//------------------- Delete a User ------------------------------------------------------
    @CacheEvict(value="usersCache",key = "#id")
    @RequestMapping(value = "/users/{id}", method = RequestMethod.DELETE)
    public ResponseEntity<User> deleteUser(@PathVariable("id") long id) {

        usersRepository.deleteById(id);
        return new ResponseEntity<User>(HttpStatus.NO_CONTENT);
    }
}

