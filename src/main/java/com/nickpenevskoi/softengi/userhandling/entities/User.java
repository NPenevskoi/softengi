package com.nickpenevskoi.softengi.userhandling.entities;

import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;
import org.springframework.cache.annotation.Cacheable;

//import javax.persistence.Cacheable;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import java.io.Serializable;

@Entity
@Cacheable
@Cache(usage=CacheConcurrencyStrategy.READ_WRITE, region="usersCache")
public class User implements Serializable {
    @javax.persistence.Id
    @GeneratedValue
    private Long id;
    @Column(nullable = false)
    private String fio;
    @Column(nullable = true)
    private String address;
    @Column(nullable = false)
    private String phoneNumber;

    public User(Long id, String fio, String address, String phoneNumber) {
        this.id = id;
        this.fio = fio;
        this.address = address;
        this.phoneNumber = phoneNumber;
    }

    public User() {
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getFio() {
        return fio;
    }

    public void setFio(String fio) {
        this.fio = fio;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getPhoneNumber() {
        return phoneNumber;
    }

    public void setPhoneNumber(String phoneNumber) {
        this.phoneNumber = phoneNumber;
    }
}
