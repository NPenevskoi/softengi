package com.nickpenevskoi.softengi.userhandling.repository;

import com.nickpenevskoi.softengi.userhandling.entities.User;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.concurrent.CompletableFuture;

public interface UsersRepository extends JpaRepository<User, Long> {

    @Query(value = "select new User(s.id, s.fio, s.address, s.phoneNumber) from User s where s.id = :id")
    CompletableFuture<User> findUserByID(@Param("id") Long id);

    @Query(value = "select new User(s.id, s.fio, s.address, s.phoneNumber) from User s where s.id = :id")
    User findUserByID_(@Param("id") Long id);

    @Query(value = "Update User s set s.fio = :fio, s.address = :address, s.phoneNumber = :phoneNumber where s.id = :id")
    CompletableFuture<User> updateUser(@Param("id") Long id, @Param("fio") String fio, @Param("address") String address, @Param("phoneNumber") String phoneNumber);

}
